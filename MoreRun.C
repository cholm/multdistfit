/**
 * @file   MoreRun.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Oct 18 11:56:47 2016
 * 
 * @brief  Run the more test. 
 */
/** 
 * Code to run the more example, see also @ref more 
 *
 * @example MoreRun.C
 */
/** 
 * Run the more test.
 * 
 * @relates MoreTest
 */
void
MoreRun(Bool_t flat=true, Bool_t same=true)
{
  gROOT->Macro("Load.C");
  gROOT->LoadMacro("MoreTest.C+g");

  Bool_t    ratio    = true;
  Bool_t    resp     = true;
  Long_t    nSamples = 1e6;
  MoreTest* test     = new MoreTest;
  test->Run("more", flat, same, ratio, resp, nSamples);

}
//
// EOF
// 
