TH2* GetResp(const char* file, const char* title)
{
  TFile* input = TFile::Open(Form("%s.root", file), "READ");
  if (!input) return 0;

  TH2* h = static_cast<TH2*>(input->Get("response"));
  if (!h) {
    Warning("GetResp", "No reponse matrix in %s", input->GetName());
    input->ls();
    input->Close();
    return 0;
  }

  h->SetDirectory(0);
  h->SetName(file);
  h->SetTitle(title);
  h->Scale(1/h->Integral());
  h->SetStats(0);
  input->Close();

  return h;
}
  
void ShowResps()
{
  TH2* flat   = GetResp("flat",   "Flat in v_{#it{I}}");
  TH2* real   = GetResp("real",   "P(v_{#it{I}})");
  TH2* redu   = GetResp("reduced","P(v_{#it{I}}), sparse");
  TH2* rewe   = static_cast<TH2*>(redu->Clone("reweighted"));
  rewe->SetTitle("P(v_{#it{I}}), sparse, reweighted");
  for (Int_t ix = 1; ix <= rewe->GetXaxis()->GetNbins(); ix++) {
    Double_t w = 1;
    if (ix <  8) w = 10;
    if (ix < 15) w =  5;
    if (ix > 35) w =  5;
    if (ix > 43) w = 10;
    for (Int_t iy = 1; iy <= rewe->GetYaxis()->GetNbins(); iy++) {
      rewe->SetBinContent(ix,iy,w*rewe->GetBinContent(ix,iy));
      rewe->SetBinError  (ix,iy,w*rewe->GetBinError  (ix,iy));
    }
  }
  rewe->Scale(1./rewe->Integral());
  rewe->SetStats(0);

  TH2* hs[] = { flat, real, redu, rewe };
      
  TCanvas* c = new TCanvas("c", "c");
  c->SetTopMargin(0.01);
  c->SetRightMargin(0.01);
  c->Divide(2,2);
  for (Int_t i = 0; i < 4; i++) {
    TVirtualPad* p = c->cd(i+1);
    p->SetPad((i%2)*.5, ((3-i)/2)*.5, (i%2+1)*.5, ((3-i)/2+1)*.5);	      
    p->SetRightMargin (i % 2 == 1 ? 0.15 : 0);
    p->SetLeftMargin  (i % 2 == 1 ? 0    : 0.15);
    p->SetBottomMargin(i < 2 ? 0    : 0.15);
    p->SetTopMargin   (i < 2 ? 0.15 : 0);
    p->SetLogz();
    hs[i]->Draw("colz");
    p->Modified();
  }
  c->Modified();
  c->Update();
  c->cd();
  c->SaveAs("reponseMatrices.pdf");
}

     
  
  
