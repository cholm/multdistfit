#
#
#
PACKAGE		:= MultDistFit
ROOT		:= root
ROOTBATCH	:= -l -b -q
ROOTFLAGS	:= -l 
DOXYGEN		:= doxygen
CODE		:= InputModel.C		\
		   SmearModel.C		\
		   ResponseModel.C	\
		   TestModel.C		\
		   PyModel.C
EXAMPLES	:= SimpleTest.C		\
		   DoubleTest.C
SCRIPTS		:= Load.C		\
		   SimpleRun.C		\
		   SimpleScan.C		\
		   DoubleRun.C
ALL		:= $(CODE) $(EXAMPLES) $(SCRIPTS) Doxyfile Makefile
ROOT6		:= $(shell \
		     echo $$(($$(root-config --version | sed 's/\..*//') > 5)))
ifeq ($(ROOT6), 1)
ROOTLOAD	= Load.C\(\"$(patsubst %Run.C,%Test,$<)\"\)
else
ROOTLOAD	=
endif

%Model_C.so:%Model.C
	$(ROOT) $(ROOTBATCH) Load.C

%Test_C.so:%Test.C $(CODE:%.C=%_C.so)
	$(ROOT) $(ROOTBATCH) Load.C\(\"$*Test\"\)

all:	$(EXAMPLES:%.C=%_C.so)

show:
	@echo "ROOT6		= $(ROOT6)"
	@echo "ROOTLOAD		= $(ROOTLOAD)"
clean:
	rm -f $(CODE:%.C=%_C.*) $(EXAMPLES:%.C=%_C.*)
	rm -f $(wildcard *.png)
	rm -rf html *~ *.pcm Dict.* MultDistFit.so a.out

simple: SimpleRun.C SimpleTest_C.so 
	$(ROOT) $(ROOTFLAGS) $(ROOTLOAD) $<

simple2: SimpleRun.C SimpleTest_C.so 
	$(ROOT) $(ROOTFLAGS) $(ROOTLOAD) $<\(false\)

double: DoubleRun.C DoubleTest_C.so 
	$(ROOT) $(ROOTFLAGS) $(ROOTLOAD) $<

scan:	SimpleScan.C SimpleTest_C.so 
	$(ROOT) $(ROOTBATCH) $(ROOTLOAD) $<

doc: Doxyfile $(CODE) $(EXAMPLES) $(SCRIPTS)
	$(DOXYGEN) $<

distdir:
	mkdir -p $(PACKAGE)
	cp $(ALL) $(PACKAGE)/

dist:	distdir
	tar -czf $(PACKAGE).tar.gz $(PACKAGE)
	rm -rf $(PACKAGE)


.PRECIOUS: $(CODE:%.C=%_C.so)

# Dict.C:$(CODE) LinkDef.h
# 	rootcling -f $@ -I../roounfold/src $^ 

# MultDistFit.so: $(CODE) Dict.C
# 	g++ -shared -fPIC -I../roounfold/src \
# 		$(shell root-config --cflags --libs) \
# 		-lUnfold ../roounfold/libRooUnfold.so $^
