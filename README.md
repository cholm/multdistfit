Test of fitting to multiplicity distributions obtained by unfolding
===================================================================

This little project sets up a test of fitting a function to a
multiplicity distribution obtained via unfolding.  The challenge is,
that it is not straight-forward to propagate the statistical
uncertainties on the measurement to the fitting routine when the final
distribution is obtained via unfolding, which inherently correlates
uncertainties. 

Content
-------

- Scripts 
  - `Load.C` Load the binary code 
  - `SimpleRun.C` Run test using the simple model 
  - `RunDouble.C` Run test using the double model 
  - `SimpleScan.C` Run scan using the simple model
- Classes 
  - `TestModel.C` Base class for tests. This code uses the *RooUnfold*
    package which must be installed. 
    - `SimpleTest.C` A simple model. It uses
	  - A negative binomial distribution for the true distribution
	  - A parameterised response model
	  - A Gaussian smear model (perpendicular to the gradient of the
        response matrix)
    - `DoubleTest.C` The double model. It uses
	  - A double negative binomial distribution for the true
        distribution
	  - A parameterised response model
	  - A Gaussian smear model (perpendicular to the gradient of the
        response matrix)
  - `InputModel.C` Models of the true distribution of particles
  - `ResponseModel.C` Models of detector responses - i.e., the mapping
    from true to measured distribution 
  - `SmearModel.C` Models of smearing the measured distribution. 
  - `FitModel.C` Models for fitting functions to the distributions
- Jupyter notebook
  - `Example.ipynb` A Jupyter notebook that illustrates the use of
    this code, including defining new tests within Python
  
Installation
------------

Make sure you have *RooUnfold* installed - see 

- http://hepunx.rl.ac.uk/~adye/software/unfold/RooUnfold.html
- https://github.com/skluth/RooUnfold

Then, edit the file `Load.C` to properly point to the installation of
*RooFit*. 

Documentation
-------------
See 

  http://cern.ch/cholm/multdistfit/
  
Run the tests
-------------

Then, you can run tests like 

    make simple
	make double
	make scan
	
The code is documented via *Doxygen*.  Run 

	make doc 
	
to generate the documentation 

_Christian_


