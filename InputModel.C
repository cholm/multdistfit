/**
 * @file   InputModel.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Oct 18 11:46:22 2016
 * 
 * @brief  Declaration and definition of input (truth) models 
 */
#ifndef INPUTMODEL_C
#define INPUTMODEL_C
#ifndef __CINT__
# include <TF1.h>
# // Below for NBD 
// # include <cmath>
# include <Math/SpecFuncMathCore.h>
#else
class TF1;
#endif


//====================================================================
/** 
 * @brief A model of the distribution of particles. 
 */
struct InputModel
{
  /** 
   * Sample the PDF 
   * 
   * @return Random sample 
   */
  virtual Double_t Sample() const = 0;
  /** 
   * Get the PDF 
   * 
   * @return Reference to internal PDF 
   */
  virtual TF1& Func() = 0;
  /** 
   * Get a copy of the PDF 
   * 
   * @param name New name 
   *
   * @return new copy of the PDF 
   */
  virtual TF1* NewFunc(const char* name) const = 0;
protected:
  /** 
   * Constructor 
   */
  InputModel() {}
  /**
   * Destructor 
   */
  virtual ~InputModel() {}
};
//____________________________________________________________________
/** 
 * @brief A negative binomial distribution of number of particles. 
 */
struct NBD : public InputModel
{
  /** 
   * Constructor 
   * 
   * @param maxN Maximum multiplicity
   * @param p    Probability 
   * @param k    Success number 
   */
  NBD(Double_t maxN, Double_t p, Double_t k)
    : fFunc("pdf",&PDF1, 0, maxN,3)
  {
    fFunc.SetParNames("A","p","k");
    fFunc.SetParameters(1,p,k);
  }
  /** Destructor */
  virtual ~NBD() {}
  /** 
   * Sample the PDF
   * 
   * @return Sample 
   */
  virtual Double_t Sample() const
  {
    return fFunc.GetRandom();
  } 
  /** 
   * Get the PDF 
   * 
   * @return Reference to internal PDF 
   */
  virtual TF1& Func() { return fFunc; }
  /** 
   * Create a new function e.g., for drawing etc. 
   * 
   * @param name The name of the new function
   * 
   * @return Pointer to newly created function object
   */
  virtual TF1* NewFunc(const char* name) const
  {
    TF1* f = new TF1(name,&PDF1, 0, fFunc.GetXmax(),fFunc.GetNpar());
    f->SetParNames("A","p","k");
    f->SetParameters(fFunc.GetParameters());
    return f;
  }
  /** 
   * Definition of negative binomial distribution (PDF) as 
   *
   * 
   @f$ \frac{\Gamma(n+k)}{\Gamma(k)\Gamma(n+1)}
   \left(\frac{m}{m+k}\right)^n\left(\frac{k}{m+k}\right)^k
   @f$ 
   *
   * where @f$ m = k(1-p)/p@f$ is the mean
   *
   * For this application, @f$ n@f$ is the independent parameter 
   * 
   * @param xp Independent parameter @f$ n@f$ 
   * @param pp Parameters 
   * 
   * @return The function evaluated at @f$ n@f$ 
   */
  static Double_t PDF1(Double_t* xp, Double_t* pp)
  {
    Double_t  x  = *xp;
    Double_t  a  = pp[0];
    Double_t  p  = pp[1];
    
    if (x <= 0) return 0;
    if (p <= 0) return 0;

    return a*InnerPDF1(xp, &(pp[1]));
  }
  static Double_t InnerPDF1(Double_t* xp, Double_t* pp)
  {
    using ROOT::Math::lgamma;
    using std::log;
    using std::exp;

    Double_t  x     = *xp;
    Double_t  p     = pp[0];
    Double_t  k     = pp[1];
    Double_t  m     = k*(1-p)/p;
    Double_t  coeff = (lgamma(x+k) - lgamma(k) - lgamma(x+1));
    return exp(coeff + x*(log(m) - log(m+k)) + k*(log(k) - log(m+k)));
  }
  /** 
   * Definition of negative binomial distribution (PDF) as 
   *
   * @f$ \frac{\Gamma(k+n)}{\Gamma(k+1)\Gamma(n)}p^n(1-p)^k@f$ 
   *
   * For this application, @f$ k@f$ is the independent parameter 
   * 
   * @param xp Independent parameter @f$ k@f$ 
   * @param pp Parameters 
   * 
   * @return The function evaluated at @f$ k@f$ 
   */
  static Double_t PDF2(Double_t* xp, Double_t* pp)
  {
    Double_t  k  = *xp;
    Double_t  a  = pp[0];
    Double_t  p  = pp[1];
    if (k <= 0 || p <= 0) return 0;

    return a*InnerPDF2(xp, &(pp[1]));
  }
  static Double_t InnerPDF2(Double_t* xp, Double_t* pp)
  {
    using ROOT::Math::lgamma;
    using std::log;
    using std::exp;

    Double_t  k  = *xp;
    Double_t  p  = pp[0];
    Double_t  n  = pp[1];

    double coeff = (lgamma(k+n) - lgamma(k+1.0) - lgamma(n));
    return exp(coeff + n * log(p) + double(k) * log(1-p));
  }
protected:
  /** Our function */
  mutable TF1 fFunc;
};  
  
//____________________________________________________________________
/** 
 * @brief A double negative binomial distribution of number of particles
 */
struct DoubleNBD : public InputModel
{
  /** 
   * Constructor 
   * 
   * @param maxN  Maximum multiplicity
   * @param alpha Relative strength 
   * @param p1    Probability 
   * @param k1    Success number 
   * @param p2    Probability 
   * @param k2    Success number 
   */
  DoubleNBD(Double_t maxN, Double_t alpha,
	    Double_t p1, Double_t k1,
	    Double_t p2, Double_t k2)
    : fFunc("pdf",&PDF, 0, maxN,6)
  {
    fFunc.SetParNames("A","#alpha", "p_{1}","k_{1}", "p_{2}", "k_{2}");
    fFunc.SetParameters(1,alpha,p1,k1,p2,k2);
  }
  /** 
   * Sample the PDF
   * 
   * @return Sample 
   */
  virtual Double_t Sample() const
  {
    return fFunc.GetRandom();
  } 
  /** 
   * Get the PDF 
   * 
   * @return Reference to internal PDF 
   */
  virtual  TF1& Func() { return fFunc; }
  /** 
   * Create a new function e.g., for drawing etc. 
   * 
   * @param name The name of the new function
   * 
   * @return Pointer to newly created function object
   */
  virtual TF1* NewFunc(const char* name) const
  {
    TF1* f = new TF1(name,PDF, 0, fFunc.GetXmax(),fFunc.GetNpar());
    f->SetParNames("A","#alpha", "p_{1}","k_{1}", "p_{2}", "k_{2}");
    f->SetParameters(fFunc.GetParameters());
    return f;
  }
protected:
  mutable TF1 fFunc;
  /** 
   * Definition of double negative binomial distribution (PDF) as 
   *
   * @f$ 
   \alpha P_{NBD}(n;p_1,k_1) + (1-\alpha)P_{NBD}(n;p_2,k_2)
   @f$ 
   *
   * For this application, @f$ n@f$ is the independent parameter 
   * 
   * @param xp Independent parameter @f$ n@f$ 
   * @param pp Parameters 
   * 
   * @return The function evaluated at @f$ n@f$ 
   */
  static Double_t PDF(Double_t* xp, Double_t* pp)
  {
    Double_t  x     = *xp;
    Double_t  a     = pp[0];
    Double_t  alpha = pp[1];

    return a*(alpha*NBD::InnerPDF1(xp,&(pp[2]))
	      +(1-alpha)*NBD::InnerPDF1(xp,&(pp[4])));
  };
};  

#endif
//
// EOF
//
