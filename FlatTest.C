/**
 * @file   FlatTest.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Mon Sep 18 13:02:18 2017
 * 
 * @brief  A test of a flat input model
 */
#include "TestModel.C"
#include "InputModel.C"
#include "SmearModel.C"
#include "ResponseModel.C"
#include "FitModel.C"

/**
 * Our input model. Draw a uniformly distributed random number between
 * 0 and given @f$ N_{\mathrm{max}}@f$
 * 
 */
struct FlatInput : public InputModel
{
  /** 
   * Constructor.
   * 
   * @param mx @f$ N_{\mathrm{max}}@f$
   */
  FlatInput(Int_t mx=30)
    : InputModel(),
      fFunc("pdf", "pol0", 0., double(mx))
  {
    fFunc.SetParameter(0,1./mx);
    fFunc.SetParName(0, "A");
  }
  /** 
   * Draw a random number from the uniform distribution
   * 
   * @return Random number
   */
  Double_t Sample() const
  {
    return fFunc.GetRandom();
  }
  /** 
   * Return reference to function implementing input distribution. 
   * 
   * @return Reference to input distribution function object
   */
  TF1& Func() { return fFunc; }
  /** 
   * Make a copy of the input distribution function object and return
   * it.
   * 
   * @param name Name of object returned
   * 
   * @return Newly allocated function object 
   */
  TF1* NewFunc(const char* name) const
  {
    TF1* f = new TF1(name, "pol0", 0., fFunc.GetXmax());
    f->SetParName(0, "A");
    f->SetParameters(fFunc.GetParameters());
    return f;
  }
  mutable TF1 fFunc;
};

/** 
 * A smearing (detector-effects) model.  This simple adds flat noise
 * to the observed value.
 */
struct FlatSmear : public SmearModel
{
  /** 
   * Smear the observed value 
   * 
   * @param output The result of @f$ Ri@f$ 
   * @param input  @f$ i@f$ - not used 
   * @param slope  @f$ dR/dI@f$ near @f$ i@f$ - not used
   * 
   * @return Smeared value of @f$ Ri@f$ 
   */
  Double_t Smear(Double_t output, Double_t& input, Double_t slope) const
  {
    return output * (1 + gRandom->Uniform(0,.1));
  }
};

/** 
 * A model of a response function.  This represents a linear response
 * with a fixed slope.
 */
struct FlatResponse : public ResponseModel
{
  /** 
   * Constructor 
   * 
   * @param smear Smearing model to use 
   */
  FlatResponse(SmearModel& smear) : fSmear(smear) {}
  /** 
   * Calculate the response to @f$ i@f$ - that is @f$ Ri@f$
   * 
   * @param input @f$ i@f$ input value - not modified
   * 
   * @return @f$ Ri@f$
   */
  Double_t ToOutput(Double_t& input) const
  {
    Double_t inp = .9*input;
    Double_t out = fSmear.Smear(inp, input, 1);
  }
  SmearModel& fSmear;
};
   
    
/** 
 * Our test of a flat input model
 */
struct FlatTest : public TestModel
{
  /** Input model */
  FlatInput    fInput;
  /** Smearing model */
  FlatSmear    fSmear;
  /** Response model */
  FlatResponse fResponse;
  /** Normal fitting */
  NormalFit    fNormalFit;
  /** Unfolded fit */
  UnfoldFit    fUnfoldFit;
  /** Constructor */
  FlatTest()
    : TestModel(),
      fInput(),
      fSmear(),
      fResponse(fSmear),
      fNormalFit(),
      fUnfoldFit()
  {
  }
  /** @return the input model - non-constant */
  InputModel& GetInputModel() { return fInput; }
  /** @return the input model - constant */
  const InputModel& GetInputModel() const { return fInput; }
  /** @return the response model - constant */
  const ResponseModel& GetResponseModel() const { return fResponse; }
  /** @return number of fit models defined */
  virtual Int_t GetNFitModels() const { return 2; }
  /** @return the @a fitno fit model defined  */
  virtual const FitModel& GetFitModel(Int_t fitno) const
  {
    switch (fitno) {
    case 0: return fNormalFit;
    case 1: return fUnfoldFit;
    }
    return fNormalFit;
  }
};
//
// EOF
//
